#!/usr/bin/env python

"""
lidar1d2.py : BreezySLAM Python with home made lidar Lidar

Copyright (C) 2017 Arnaud Joset inspired by Simon D. Levy
https://github.com/simondlevy/BreezySLAM/

This code is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

This code is distributed in the hope that it will be useful,     
but WITHOUT ANY WARRANTY without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License 
along with this code.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import os
import ast
import sleekxmpp

logger = logging.getLogger(__name__)

################################################


class LidarBot(sleekxmpp.ClientXMPP):
    """
    A basic Sleekxmpp bot that will log in, send a message,
    and then log out.
    """

    def __init__(self, jid, password, recipient):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)

        # The message we wish to send, and the JID that
        # will receive it.
        self.recipient = recipient + '/lidar'
        self.previous_scan = [0,[]]
        self.scan = [0,[]]
        self.data = []

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)
        self.add_event_handler("message", self.message)


    def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
                     :param event:
        """
        self.send_presence()
        self.get_roster()


        # self.disconnect()

    def send_msg(self, msg):
        self.send_message(mto=self.recipient,
                          mbody=msg,
                          mtype='chat')  # normal or chat

    def message(self, msg):
        """
        Process incoming message stanzas. Be aware that this also
        includes MUC messages and error messages. It is usually
        a good idea to check the messages's type before processing
        or sending replies.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
                   :param msg: the message
        """
        if msg['type'] in ('chat', 'normal'):
            message = msg['body']
            #logger.critical(message)
            #self.send_msg("DATA Recieved !")
            if '#' in message:
                scan = message.split('#')
                if scan[0] != self.previous_scan[0]:

                    data_str = scan[1]
                    data = ast.literal_eval(data_str)
                    #print(data)
                    self.data = data
                    self.scan = scan
    def get_data(self):
        return self.data
    def reset_data(self, a = None):
        self.data = a

################################################
def read_parameters(file):
    with open(file, "r") as f:
        parameters = []
        for line in f:
            line = line.rstrip('\r\n')
            parameters.append(line)
        jid = parameters[0] + '/lidar'
        password = parameters[1]
        to = parameters[2]
        return jid, password, to


def rocket_start(user):
    file = None
    filename = 'send_xmpprc_' + user
    if os.path.exists(filename):
        file = 'send_xmpprc_' + user
    elif os.path.exists('/home/arnaud/.config/xmpp/' + filename):
        file = '/home/arnaud/.config/xmpp/' + filename
    try:
        jid, password, to = read_parameters(file)
    except TypeError:
        logger.critical("Config file not found")
        return "Config file not found"
    logging.info("Start")
    #    print(jid,password, to)
    if jid is None or password is None or to is None:
        print("Error in config file")
        logging.error("Error in config file")
        return "Error config not found"
    xmpp = LidarBot(jid, password, to)
    xmpp.register_plugin('xep_0030')  # Service Discovery
    xmpp.register_plugin('xep_0199')  # XMPP Ping
    xmpp.connect()
    if xmpp.connect():
        # block=True will block the current thread. By default, block=False
        xmpp.process()
    else:
        print("Unable to connect.")
    xmpp.send_msg("Lidar en ligne")
    return xmpp


if __name__ == "__main__":
    xmpp_instance = rocket_start()
