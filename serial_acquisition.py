#!/usr/bin/env python

"""
lidar1d2.py : BreezySLAM Python with home made lidar Lidar

Copyright (C) 2017 Arnaud Joset inspired by Simon D. Levy
https://github.com/simondlevy/BreezySLAM/

This code is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

This code is distributed in the hope that it will be useful,     
but WITHOUT ANY WARRANTY without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License 
along with this code.  If not, see <http://www.gnu.org/licenses/>.
"""
import serial
import logging
import pandas as pd
from lidarnaud import xmpp
import time
try:
    import Adafruit_CharLCD as Lcd
except ImportError:
    Lcd = None
logging.basicConfig()
logger = logging.getLogger("serial_parser")
logger.setLevel("DEBUG")


DEVICE = '/dev/ttyUSB0'
QUALITY = 32767
ser = serial.Serial(DEVICE, 230400)


def read_serial():
    ret = ser.readline()
    return ret


class MyCharPlate(Lcd.Adafruit_CharLCDPlate):
    # LED colors
    RED = (1.0, 0.0, 0.0)
    GREEN = (0.0, 1.0, 0.0)
    BLUE = (0.0, 0.0, 1.0)
    YELLOW = (1.0, 1.0, 0.0)
    CYAN = (0.0, 1.0, 1.0)
    MAGENTA = (1.0, 0.0, 1.0)
    WHITE = (1.0, 1.0, 1.0)
    # BUTTONS
    SELECT = Lcd.SELECT
    LEFT = Lcd.LEFT
    UP = Lcd.UP
    DOWN = Lcd.DOWN
    RIGHT = Lcd.RIGHT

    def color(self, color):
        red, green, blue = color
        self.set_color(red, green, blue)


def process_data(line):
    """
    process the data obtained by serial communuication
    :param line: serial data: """
    # logger.debug(line)
    line = line.decode('utf-8')
    try:
        txt_data = line.split(";")
    except TypeError:
        # No data
        return None
    if "EOR" in line:
        txt_data = line.split(";")
        count = float(txt_data[1])
        local_count = float(txt_data[2])
        timer = float(txt_data[2])
        distance = 0
        line_data = [count, local_count, distance, timer, 0]
        msg = "New rotation,  count : {} timer : {}".format(count, timer)
        logger.debug(msg)
    elif ";" in line:
        count = float(txt_data[0])
        local_count = float(txt_data[1])
        timer = float(txt_data[3])
        distance = float(txt_data[2])
        line_data = [count, local_count, distance, timer, 0]
    else:
        return None
    return line_data


def super_loop(df):
    txt = ''
    i = 0
    while 'EOR' not in str(txt):
        msg = "Count: {}".format(i)
        # logger.debug(msg)
        txt = read_serial()
        line_data = process_data(txt)
        if line_data:
            if line_data[0] < 361:
                df.loc[i] = line_data
        i += 1
    return df


if __name__ == "__main__":
    columns = ("count", "local_count", "distance", "timer", "new_rotation")
    df = pd.DataFrame(columns=columns)
    global_df = pd.DataFrame(columns=columns)
    xmpp_instance = xmpp.rocket_start('r1d2')
    lcd_instance = MyCharPlate(busnum=1)
    lcd_instance.clear()
    lcd_instance.color(lcd_instance.YELLOW)
    lcd_instance.message("Starting Lidar\nR1D2")
    time.sleep(2.5)
    lcd_instance.clear()
    color = lcd_instance.CYAN
    lcd_instance.color(color)
    for t in range(500):
        try:
            df = super_loop(df)
            logger.info(t)
            global_df = global_df.append(df, ignore_index=True)
            # df.to_csv("lidar_partial.csv", sep=";", encoding='utf-8', index_label='index')
            # Send sleekxmpp data
            data = []
            df.reset_index()
            for index, row in df.iterrows():
                angle = row['count']
                distance = row['distance']
                if distance > 0:
                    # tulpe = distance, quality (16 bits), angle for debugging purpose
                    data.append((distance, QUALITY, angle))
                    # data.append(distance)
            msg = str(time.time()) + '#' + str(data)
            xmpp_instance.send_msg(msg)
            lcd_instance.clear()
            lcd_instance.message("Sending data {}".format(t))
            # LCD update print t
            a = 0
        except KeyboardInterrupt:
            logger.critical("Keyboard Interrupt")
            t = 501
    global_df.to_csv("lidar.csv", sep=";", encoding='utf-8', index_label='index')
