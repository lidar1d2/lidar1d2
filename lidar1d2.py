#!/usr/bin/env python

"""
lidar1d2.py : BreezySLAM Python with home made lidar Lidar
                 
Copyright (C) 2017 Arnaud Joset inspired by Simon D. Levy
https://github.com/simondlevy/BreezySLAM/

This code is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

This code is distributed in the hope that it will be useful,     
but WITHOUT ANY WARRANTY without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License 
along with this code.  If not, see <http://www.gnu.org/licenses/>.
"""


import ast
import logging
import time
from statistics import mean, StatisticsError

from PIL import Image
from breezyslam.algorithms import RMHC_SLAM
from breezyslam.algorithms import Deterministic_SLAM
from breezyslam.components import Laser
from breezyslam.robots import WheeledRobot

from lidarnaud import xmpp
from pltslamshow import SlamShow

# from xvlidar import XVLidar as Lidar
# from lidarnaud.lidarnaud import Lidarnaud as Lidar

MAP_SIZE_PIXELS = 500
MAP_SIZE_METERS = 4
LIDAR_DEVICE = '/dev/ttyUSB1'
QUALITY = 32767

logging.basicConfig()
logger = logging.getLogger("lidar R1D2")
logger.setLevel("ERROR")


class Rover(WheeledRobot):
    def __init__(self):
        """
                wheelRadiusMillimeters    radius of each odometry wheel   
                halfAxleLengthMillimeters half the length of the axle between the odometry wheels  
                """
        WheeledRobot.__init__(self, 77, 165)

        self.ticks_per_cycle = 2000

    def __str__(self):
        return '<%s ticks_per_cycle=%d>' % (WheeledRobot.__str__(self), self.ticks_per_cycle)

    def computeVelocities(self, odometry, **kwargs):
        """
                Computes forward and angular velocities based on odometry.

                Parameters:

                  timestamp          time stamp, in whatever units your robot uses       
                  leftWheelOdometry  odometry for left wheel, in whatever form your robot uses       
                  rightWheelOdometry odometry for right wheel, in whatever form your robot uses

                Returns a tuple (dxyMillimeters, dthetaDegrees, dtSeconds)

                  dxyMillimeters     forward distance traveled, in millimeters
                  dthetaDegrees change in angular position, in degrees
                  dtSeconds     elapsed time since previous odometry, in seconds
                  :param **kwargs: 
                """
        return WheeledRobot.computeVelocities(self, odometry[0], odometry[1], odometry[2])

    def extractOdometry(self, timestamp, leftWheel, rightWheel):
        # Convert microseconds to seconds, ticks to angles
        return timestamp / 1e6, \
               self._ticks_to_degrees(leftWheel), \
               self._ticks_to_degrees(rightWheel)

    def odometryStr(self, odometry):
        return '<timestamp=%d usec leftWheelTicks=%d rightWheelTicks=%d>' % \
               (odometry[0], odometry[1], odometry[2])

    def _ticks_to_degrees(self, ticks):
        return ticks * (180. / self.ticks_per_cycle)


# Helpers ---------------------------------------------------------

def mm2pix(mm):
    return int(mm / (MAP_SIZE_METERS * 1000. / MAP_SIZE_PIXELS))

# ---------------------------------------------------------
class Lidarnaud(Laser):
    """
    A class for the Lidarnaud, a home made lidar
    """

    def __init__(self, detectionMargin=0, offsetMillimeters=0):
        scan_size = 360
        scan_rate_hz = 10.0
        detection_angle_degrees = 360
        distance_no_detection_mm = 6000  # 20000
        Laser.__init__(self, scan_size, scan_rate_hz, detection_angle_degrees, distance_no_detection_mm,
                       detectionMargin, offsetMillimeters)


def open_lidar_csv(filename):
    """
    This function open a csv file containing all the data
    :return: table of lidar data: Returns 307 (distance, quality) tuples.
    """
    table_slam = []
    with open(filename) as filetest:
        for line in filetest:
            data = ast.literal_eval(line)
            # toks = line.split()[0:-1] # ignore ''
            # timestamp = int(toks[0])
            # odometry = timestamp, int(toks[2]), int(toks[3])
            # lidar = [float(tok) for tok in data]
            # print(lidar)
            table_slam.append(data)
    return table_slam


def fix_duplicates(table_slam):
    """
    Fix the duplicates in the 360 degree table
    :param table_slam: table with duplicates
    :return:fixed_table_slam without duplicates: the average of multiple angle are calculated
    """
    fixed_table_slam = [None] * 360
    fixed_dict = {i: {'full': [], 'fixed': None} for i in range(1, 361)}
    for key, value in fixed_dict.items():
        for triplet in table_slam:
            try:
                if float(triplet[2]) == float(key):
                    value['full'].append(triplet[0])
            except IndexError:
                logger.error("Index error : {}".format(triplet, key))
                pass
    for key, value in fixed_dict.items():
        try:
            value['fixed'] = mean(value['full'])
            fixed_table_slam[key - 1] = (value['fixed'], QUALITY, key)
        except StatisticsError:
            print(key, value)
            print(table_slam)
            return None

    return fixed_table_slam


def fix_data(scan_data):
    """
    Fix the missing values of the 360 degree rotation.
    The fixed table have sometimes multiples values for one angle, resulting of a table size > 360
    :param scan_data: the table to fix
    :return: the fixed table
    """
    fixed_scan_data = scan_data
    # print(table_slam)
    angles = [float(i) for i in range(1, 361)]
    known_angles = [i[2] for i in scan_data]
    # print(known_angles)
    for a in angles:
        if a in known_angles:
            try:
                angles.remove(a)
            except ValueError:
                pass
    # print(angles)
    # Add empty values for non existent angles
    for a in angles:
        #previous_data = [item for item in fixed_scan_data if item[2] == a-1]
        #fixed_scan_data.append((previous_data[0][0], QUALITY, a))
        #b = 0
        fixed_scan_data.append((1, QUALITY, a))
    return fixed_scan_data


def test(file=None, slam=None, display=None, mapbytes=None, xmpp_instance=None,
         log_file='log.csv', robot=None):
    count = 0
    previous_timestamp = 0
    data = []
    if file:
        table_slam = open_lidar_csv(file)
    while True:
        if file:
            logger.info(file)
            logger.info("get data")
            try:
                data_file = table_slam[count]
                count += 1
            except IndexError:
                logger.error("Last line reached:  {}".format(file))
                time.sleep(10)
                break
            logger.info("Fix empty values")
            data_file = fix_data(data_file)
            logger.info("Average multiples values")
            data_file = fix_duplicates(data_file)
            # logger.debug(table_slam)
            data = data_file

        elif xmpp_instance:
            logger.info("get data")
            timestamp = float(xmpp_instance.scan[0])
            data = None
            if timestamp != previous_timestamp:
                previous_timestamp = timestamp
                data_xmpp = xmpp_instance.get_data()
                if len(data_xmpp) > 360 or data_xmpp is None:
                    continue
                xmpp_instance.reset_data(None)
                logger.info("Fix empty values")
                data_xmpp = fix_data(data_xmpp)
                logger.info("Average multiples values")
                data_xmpp = fix_duplicates(data_xmpp)
                logger.debug('data xmpp ' + str(data_xmpp))
                with open(log_file, mode='a') as logfile:
                    data_log = str(data_xmpp)
                    # data_log = data.replace('[', '')
                    # data_log = data.replace((']'), '')
                    # data_log = data.replace(',', '')
                    logfile.writelines(data_log + '\n')

                data = data_xmpp
        if data:
            # Update SLAM with current Lidar scan, using first element of (scan, quality) triplets
            # Warning: if distance is too short, it will be null and nothing will be display
            update = [triplet[0] * 2 for triplet in data]
            #update = [1000 for triplet in data]
            logger.debug(len(update))
            timestamp_dumb = time.time()
            odometry = [timestamp_dumb, 25, 250]
            velocities = robot.computeVelocities(odometry)
            slam.update(update, velocities)
            # Get current robot position
            x_mm, y_mm, theta_degrees = slam.getpos()
            trajectory.append((x_mm, y_mm))
            # Get current map bytes as grayscale
            slam.getmap(mapbytes)
            display.displayMap(mapbytes)
            display.setPose(x_mm, y_mm, theta_degrees)

            # Put trajectory into map as black pixels
            for coords in trajectory:
                x_mm, y_mm = coords

                x_pix = mm2pix(x_mm)
                y_pix = mm2pix(y_mm)

                mapbytes[y_pix * MAP_SIZE_PIXELS + x_pix] = 0

            # Exit on ESCape
            key = display.refresh()

            if key is not None and (key & 0x1A):
                # Save map and trajectory as PNG file
                exit(0)
            image = Image.frombuffer('L', (MAP_SIZE_PIXELS, MAP_SIZE_PIXELS), mapbytes, 'raw', 'L', 0, 1)
            image.save('test.png')
        else:
            logger.debug("DATA {}".format(data))
        time.sleep(0.25)


if __name__ == '__main__':
    # Connect to Lidar unit
    # lidar = Lidar(LIDAR_DEVICE)

    # Create an RMHC SLAM object with a laser model and optional robot model
    #slam = RMHC_SLAM(Lidarnaud(), MAP_SIZE_PIXELS, MAP_SIZE_METERS, random_seed=42)
    slam = Deterministic_SLAM(Lidarnaud(), MAP_SIZE_PIXELS, MAP_SIZE_METERS)

    # Set up a SLAM display
    display = SlamShow(MAP_SIZE_PIXELS, MAP_SIZE_METERS * 1000 / MAP_SIZE_PIXELS, 'SLAM')

    # Initialize an empty trajectory
    trajectory = []

    # Initialize empty map
    mapbytes = bytearray(MAP_SIZE_PIXELS * MAP_SIZE_PIXELS)

    robot = Rover()
    print(robot)

    file = True
    xmpp_instance = False
    if file:
        file = 'test.csv'
    if xmpp_instance:
        xmpp_instance = xmpp.rocket_start('jnanar')
    test(file=file, slam=slam, display=display, mapbytes=mapbytes, xmpp_instance=xmpp_instance,
         log_file='log.csv', robot=robot)
